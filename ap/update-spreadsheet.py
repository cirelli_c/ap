import argparse
import gspread
import time
import os

# from: https://stackoverflow.com/questions/23861680/convert-spreadsheet-number-to-column-letter
def num_to_col_letters(num):
    letters = ''
    while num:
       mod = (num - 1) % 26
       letters += chr(mod + 65)
       num = (num - 1) // 26
    return ''.join(reversed(letters))

backgroundColorTitle={ "backgroundColor": { "red" : 182./255., "green" : 215./255., "blue" : 168./255.} }

parser = argparse.ArgumentParser()
parser.add_argument("--credentials", default="credentials-1.json", help="json file with credentials for spreadsheet. https://gspread.readthedocs.io/en/latest/oauth2.html")
parser.add_argument("--worksheet",   default="Logbook",        help="name of worksheet, default Stats")
parser.add_argument("--url",         default=None,               help="url to spreadsheet")

parser.add_argument("--setup",       action='store_true',        help="prepare spearsheet for experiment")

parser.add_argument("--unique_run",             default=None, type=int,  help="unique run number, default None")
parser.add_argument("--laser",           default="dark",          help="dark or light, default dark")
parser.add_argument("--row",             default=2,    type=int,  help="row number with the names of the variables. default=2")

parser.add_argument("--number_frames",   default=None, type=int, help="Number of frames")
parser.add_argument("--hits_rate",       default=None, type=float, help="Hits rate")
parser.add_argument("--number_indexed",  default=None, type=int, help="Number of indexed")
parser.add_argument("--number_indexed_alternative",  default=None, type=int, help="Number of indexed in alternative indexing")
parser.add_argument("--resolution_max",  default=None,           help="Resolution max")
parser.add_argument("--resolution_min",  default=None,           help="Resolution min")
parser.add_argument("--resolution_mean", default=None,           help="Resolution mean")

parser.add_argument("--user_tag",       default=None,           help="User tag")
parser.add_argument("--time_run_taken", default=None,           help="When run was started")
parser.add_argument("--run_number",     default=None,           help="run number")
parser.add_argument("--acq_number",     default=None,           help="acqusition number")

parser.add_argument("--cell_name",      default=None,           help="Name of cell(protein) to use for automatic indexing")

parser.add_argument("--motor_name",     default=None,           help="motor name")
parser.add_argument("--motor_value",    default=None,           help="motor value")

args = parser.parse_args()

if not os.path.isfile(args.credentials):
    print("no %s credential file, exit" % args.credentials)
    exit(1) 

possible_row_run_names = ['RUN number', 'Run #']

what_to_insert_laser = {"# Frames": args.number_frames, "HitsRate": args.hits_rate, "indexed #": args.number_indexed, 
                                                                                    "alternative indexed #": args.number_indexed_alternative,
                  "resolution max": args.resolution_max, "resolution min": args.resolution_min, "resolution mean": args.resolution_mean}

what_to_insert = {"run number": args.run_number, "acq number": args.acq_number, "User tag": args.user_tag, "Cell": args.cell_name, "Time": args.time_run_taken, "Motor name": args.motor_name, "Motor value": args.motor_value}

what_to_insert_in_summary = ["User tag", "First RUN number", "Last RUN number", "index rate dark", "index rate light", "total images light", "indexed images light","total images dark", "indexed images dark", "row number first", "row number last"]



what_to_find = ["User tag", "RUN number", "# Frames light", "indexed # light", "# Frames dark", "indexed # dark"]
lett_col = []

summary = None
itry = 0
ierror = 1
while itry<2 and ierror == 1:
    itry += 1
    ierror = 0
    try:
        gc = gspread.service_account(filename=args.credentials)

        if args.url:
            spreadsheet = gc.open_by_url(args.url)
        else:
            print("no spreadsheet defined, exit")
            exit(1)
        worksheet_list = spreadsheet.worksheets()
        worksheet_found = False
        summary_found = False

        for w in worksheet_list:
            if w.title == args.worksheet:
                worksheet_found = True
            if w.title == 'Datasets summary':
                summary_found = True
        if not worksheet_found:
            if not args.setup:
                print("worksheet %s not found in spreasheet" % args.worksheet)
                exit(1)
            worksheet = spreadsheet.add_worksheet(title=args.worksheet,rows="10000",cols="100")
        else:
            worksheet   = spreadsheet.worksheet(args.worksheet)
        if not summary_found:
            summary = spreadsheet.add_worksheet(title='Datasets summary',rows="100",cols="100")
    except Exception as e: 
        ierror = 1
    time.sleep(2)

if ierror == 1:
    print("Error in opening google spreadsheet")
    exit(1)

row_names = []
row_names_summary = []
formula_names_summary = []

try:
    row_names             = worksheet.row_values(args.row)

    if summary:
        row_names_summary     = summary.row_values(1)
        formula_names_summary     = summary.row_values(2)
except:
    pass

row_run = None

for run_row_name in possible_row_run_names:
    if run_row_name in row_names:
        row_run = row_names.index(run_row_name)+1

if row_run == None and args.setup:
    row_run = len(row_names)+1
    row_run_name = num_to_col_letters(row_run)

    worksheet.update_cell(args.row, row_run, possible_row_run_names[0])
    worksheet.format(f'{row_run_name}{args.row}', backgroundColorTitle)

    N_RUNS_INITIAL=5000
    r_ins = [ [i] for i in range(1,1+N_RUNS_INITIAL+1)]
    worksheet.update(f'{row_run_name}{args.row+1}:{row_run_name}{args.row+1+N_RUNS_INITIAL}', r_ins)

    time.sleep(10)

if row_run != None:
    column_names = worksheet.col_values(row_run)
else:
    print("Cannot find row with the pre-defined run numbers")
    exit()

if args.setup:

    for wht in what_to_insert:
        if wht not in row_names:
            wht_col = len(worksheet.row_values(args.row))+1
            worksheet.update_cell(args.row, wht_col, wht)
            wht_col_name = num_to_col_letters(wht_col)
            worksheet.format(f'{wht_col_name}{args.row}', backgroundColorTitle)

    time.sleep(10)

    for laser in ['dark', 'light']:
        for wht in what_to_insert_laser:
            wht = wht + " " + laser
            if wht not in row_names:
                wht_col = len(worksheet.row_values(args.row))+1
                worksheet.update_cell(args.row, wht_col, wht)
                wht_col_name = num_to_col_letters(wht_col)
                worksheet.format(f'{wht_col_name}{args.row}', backgroundColorTitle)

    time.sleep(10)

    if summary:
        for index, wht in enumerate(what_to_insert_in_summary):
            if wht not in row_names_summary:
                summary.update_cell(1, index+1, wht)
                if index < len(what_to_insert_in_summary)-2:
                    wht_col_name = num_to_col_letters(index+1)
                    summary.format(f'{wht_col_name}1', backgroundColorTitle)

        for wht in what_to_find:
            cell = worksheet.find(wht) 
            lett_col.append(num_to_col_letters(cell.col))

        formulas_in_summary = ["=INDIRECT(\"" + args.worksheet + "!\"&\"" + lett_col[0] + "\"&J2 &\"\")", 
                               "=INDIRECT(\"" + args.worksheet + "!\"&\"" + lett_col[1] + "\"&J2 &\"\")", 
                               "=INDIRECT(\"" + args.worksheet + "!\"&\"" + lett_col[1] + "\"&K2 &\"\")",
                               "=I2/(H2+0.001)", 
                               "=G2/(F2+0.001)", 
                               "=SUM(INDIRECT(\"" + args.worksheet + "!\"&\"" + lett_col[2] + "\"&J2 &\"\"):INDIRECT(\"" + args.worksheet + "!\"&\"" + lett_col[2] + "\"&K2 &\"\"))", 
                               "=SUM(INDIRECT(\"" + args.worksheet + "!\"&\"" + lett_col[3] + "\"&J2 &\"\"):INDIRECT(\"" + args.worksheet + "!\"&\"" + lett_col[3] + "\"&K2 &\"\"))", 
                               "=SUM(INDIRECT(\"" + args.worksheet + "!\"&\"" + lett_col[4] + "\"&J2 &\"\"):INDIRECT(\"" + args.worksheet + "!\"&\"" + lett_col[4] + "\"&K2 &\"\"))", 
                               "=SUM(INDIRECT(\"" + args.worksheet + "!\"&\"" + lett_col[5] + "\"&J2 &\"\"):INDIRECT(\"" + args.worksheet + "!\"&\"" + lett_col[5] + "\"&K2 &\"\"))"]

        time.sleep(10)

        for index, wht in enumerate(formulas_in_summary):
            if wht not in formula_names_summary:
                summary.update_cell(2, index+1, wht)

    exit()

if not args.unique_run and not args.setup:
    print("there should be run number, with --unique_run provided")
    exit(1)

run_number = str(args.unique_run)

if run_number in column_names:
    column_index = column_names.index(run_number)+1
else:
    print("Run %s not found in spreadsheet" % run_number)
    exit()

for wht in what_to_insert_laser: 
    if what_to_insert_laser[wht] != None:
        name = wht+" "+args.laser
        if name in row_names:
            insert_row = row_names.index(name)+1
        else:
            print("%s not found in the table" % name)
            exit()
        worksheet.update_cell(column_index, insert_row, what_to_insert_laser[wht])

for wht in what_to_insert:
    if what_to_insert[wht] != None:
        name = wht
        if name in row_names:
            insert_row = row_names.index(name)+1
        else:
            print("%s not found in the table" % name)
            exit()
        worksheet.update_cell(column_index, insert_row, what_to_insert[wht])


