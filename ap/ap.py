import argparse
import time
import os
import json
from datetime import datetime
from subprocess import Popen
from random import choice
from glob import glob

parser = argparse.ArgumentParser()

parser.add_argument("--beamline",  default="alvra",           help="beamline")
parser.add_argument("--pgroup",    default="p18674",          help="pgroup")
parser.add_argument("--detector",  default="JF06T08V02",      help="detector")
parser.add_argument("--logbook",   default=None,              help="url to logbook")
parser.add_argument("--dir_ap",    default=None,              help="path to ap code")

args = parser.parse_args()

beamline = args.beamline
pgroup   = args.pgroup
detector = args.detector
logbook_url = args.logbook
dir_ap = args.dir_ap

credential_files = glob("credentials-*.json")

raw_directory = f'/sf/{beamline}/data/{pgroup}/raw'
if not os.path.exists(raw_directory):
    print(f'Something wrong with beamline pgroup ({raw_directory}) or permision')
    exit()

last_run_file = f'{raw_directory}/run_info/LAST_RUN'
if not os.path.exists(last_run_file):
    print(f'last run file does not exists {last_run_file}')
    last_run=0
else:
    with open(last_run_file, "r") as run_file:
        last_run = int(run_file.read())

for run in range(last_run,0,-1):

    data_directory = glob(f'{raw_directory}/run{run:04}*')

    if len(data_directory) != 1:
        continue

    data_directory=data_directory[0]

    for run_info_file in glob(f'{data_directory}/meta/acq*'):
        if not os.path.exists(run_info_file):
            continue
        try:
            with open(run_info_file) as json_file:
                run_parameters = json.load(json_file)
        except:
            continue

        unique_run_number = run_parameters["unique_acquisition_run_number"]

        log_file = f'output/run{unique_run_number:06}.base'
        if os.path.exists(log_file):
            continue

        start_pulse_id = run_parameters["start_pulseid"]
        stop_pulse_id  = run_parameters["stop_pulseid"]
 
        rate_multiplicator = run_parameters.get("rate_multiplicator", 1)
        detector_rate = 100//rate_multiplicator
   
        user_tag = run_parameters.get("user_tag", None)
        run_number = run_parameters["run_number"]
        acq_number = run_parameters.get("acquisition_number", None)

        cell_name = run_parameters.get("cell_name", "")
        if cell_name == "":
            cell_name = "no_cell"

        motor_name  = None
        motor_value = None
        if "scan_info" in run_parameters:
            motor_name = run_parameters["scan_info"].get("name", None)       
            motor_value = run_parameters["scan_info"].get("scan_readbacks", None)
 
        request_time = run_parameters.get("request_time", str(datetime.now()))
        try:
            request_time = datetime.strptime(request_time, '%Y-%m-%d %H:%M:%S.%f')
        except:
            request_time = datetime.strptime(request_time, '%Y-%m-%d %H:%M:%S')

        now = datetime.now()
        if (now-request_time).total_seconds() < 30:
            time.sleep(30)

        trun = request_time.strftime('%m-%d_%H:%M')

        process_log_file = open(f'output/run{unique_run_number:06}.base.out', 'w')

        f_log = open(log_file, "w")

        if logbook_url is not None:
            credential_file = choice(credential_files)

            log_run = f'python {dir_ap}/ap/update-spreadsheet.py --url {logbook_url} --unique_run {unique_run_number} --run_number {run_number} --acq_number {acq_number} --user_tag {user_tag} --cell_name {cell_name} --time_run_taken {trun} --motor_name {motor_name} --motor_value {motor_value} --credentials {credential_file}'
            process=Popen(log_run, shell=True, stdout=process_log_file, stderr=process_log_file)
            print(log_run, file = f_log)

        f_log.close()

        process_log_file.close()

        time.sleep(3)

if os.path.exists("NO_INDEXING"):
    print("NO_INDEXING file present, do not run indexing")
    exit()

for run in range(1, last_run+1):

    data_directory = glob(f'{raw_directory}/run{run:04}*')

    if len(data_directory) != 1:
        continue

    data_directory=data_directory[0]
    for run_info_file in glob(f'{data_directory}/meta/acq*'):
        if not os.path.exists(run_info_file):
            continue
        try:
            with open(run_info_file) as json_file:
                run_parameters = json.load(json_file)
        except:
            continue

        unique_run_number = run_parameters["unique_acquisition_run_number"]

        for laser in ["dark", "light"]:

            log_file = f'output/run{unique_run_number:06}.index.{laser}'

            if os.path.exists(log_file):
                continue

            dir_name = data_directory.split("/")[-1]
            acq_number = run_parameters.get("acquisition_number", None)

            user_tag = run_parameters.get("user_tag", None)

            cell_name = run_parameters.get("cell_name", "")
            if cell_name == "":
                cell_name = "no_cell"

            start_pulse_id = run_parameters["start_pulseid"]
            stop_pulse_id  = run_parameters["stop_pulseid"]

            dap_file = f"{data_directory}/data/acq{acq_number:04}.{detector}.dap"

            frame_list_file = f'{data_directory}/data/acq{acq_number:04}.{detector}.{laser}.lst'

            if os.path.exists(frame_list_file):

                all_dark   = []
                all_light  = []
                hits_dark  = []
                hits_light = []

                if os.path.exists(dap_file):

                    with open(dap_file, 'r') as hits_file:
                        all_lines = hits_file.read().splitlines()
                        for line in all_lines:
                            lp = line.split()
                            pulseid, frame_good, is_frame_hit, laser_on = lp[0], lp[1], lp[2], lp[4]
                            pulseid = int(pulseid)
                            if pulseid < start_pulse_id or pulseid > stop_pulse_id:
                                continue
                            is_frame_hit = True if is_frame_hit == "True" else False
                            laser_on = True if laser_on == "True" else False
                            if laser_on:
                                all_light.append(pulseid)
                            else:
                                all_dark.append(pulseid)
                            if is_frame_hit:
                                if laser_on:
                                    hits_light.append(pulseid)
                                else:
                                    hits_dark.append(pulseid)

                hitrate_dark = len(hits_dark)/(len(all_dark)+0.01)
                hitrate_light = len(hits_light)/(len(all_light)+0.01)

                if laser == "dark":
                    hitrate_value = hitrate_dark
                else:
                    hitrate_value = hitrate_light

                nframes = 0
                with open(frame_list_file, "r") as fr_file:
                    nframes= len(fr_file.readlines())

                process_log_file = open(f'output/run{unique_run_number:06}.index.{laser}.out', 'w')

                f_log = open(log_file, "w")

                if logbook_url is not None:
                    credential_file = choice(credential_files)

                    log_laser = f'python {dir_ap}/ap/update-spreadsheet.py --url {logbook_url} --unique_run {unique_run_number} --number_frames {nframes} --hits_rate {hitrate_value} --laser {laser} --credentials {credential_file}'
                    process=Popen(log_laser, shell=True, stdout=process_log_file, stderr=process_log_file)
                    print(log_laser, file = f_log)

                #slurm_partition = choice(["prod-aramis", "prod-aramis", "prod-aramis", "prod-athos"])
                slurm_partition = choice(["prod-aramis", "prod-aramis", "prod-aramis"])

                log_index = f'sbatch --exclusive -p {slurm_partition} -J {unique_run_number}_{laser} -e output/run{unique_run_number:06}.index.{laser}.slurm.err -o output/run{unique_run_number:06}.index.{laser}.slurm.out {dir_ap}/scripts/index_data.sh {dir_name}/index/{laser} {acq_number} {frame_list_file} {cell_name} {unique_run_number}'
                print(log_index, file = f_log)
                process=Popen(log_index, shell=True, stdout=process_log_file, stderr=process_log_file)

                f_log.close()
