#!/bin/bash

rm -rf CURRENT_JOBS.txt
echo " statistics at "`date` >> CURRENT_JOBS.txt
echo "   Running jobs " >> CURRENT_JOBS.txt
squeue | grep " R " | awk '{print $3" "$6}' >> CURRENT_JOBS.txt
echo " " >> CURRENT_JOBS.txt
echo "   Pending jobs " >> CURRENT_JOBS.txt
squeue | grep " PD " | awk '{print $3}' >> CURRENT_JOBS.txt

