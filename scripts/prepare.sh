#!/bin/bash

SRC=`dirname ${BASH_SOURCE}`

NAME_OF_AP_CONFIG_DIRECTORY=ap_config

if [ -e ${NAME_OF_AP_CONFIG_DIRECTORY} ]
then
    echo "${NAME_OF_AP_CONFIG_DIRECTORY} exist, please re-name that before re-running this script again"
    exit
fi

mkdir ${NAME_OF_AP_CONFIG_DIRECTORY}
chmod g+w ${NAME_OF_AP_CONFIG_DIRECTORY}
cd ${NAME_OF_AP_CONFIG_DIRECTORY}

for d in CELL output
do
    if [ ! -d ${d} ]
    then
        mkdir ${d} 
        chmod -R g+w ${d}
    fi
done

for f in env_setup.sh run_index.sh
do
    if [ ! -e $f ]
    then
        cp ${SRC}/$f $f
    fi
done

#path to current directory which contains config files
echo "export CONFIG_DIR=$PWD" >> env_setup.sh
# path to the installed ap package
DIR_AP=`dirname ${SRC}`
echo "export DIR_AP=${DIR_AP}" >> env_setup.sh

echo "Edit file env_setup.sh with correct information (PGROUP, DETECTOR_NAME, BEAMLINE, LOGBOOK_URL)"

echo "Put proper geom file as <DETECTOR_NAME>.geom (e.g. JF17T16V01.geom)"

echo "Copy cell files to CELL/ directory (example : lyso.cell, hewl.cell....)"

echo "Make files credentials.json, credentials-*.json (e.g. credentials-1.json, credentials-2.json) with the api keys to access logbook"

if [ ! -e DETECTOR_DISTANCE.txt ]
then
    echo "DEFAULT 0.09369" > DETECTOR_DISTANCE.txt
    echo Make some good guess for detector distance in DETECTOR_DISTANCE.txt file
fi

if [ ! -e BEAM_ENERGY.txt ]
then
    echo "DEFAULT 11330.0" > BEAM_ENERGY.txt
    echo Make proper beam energy in BEAM_ENERGY.txt file
fi

for f in env_setup.sh run_index.sh DETECTOR_DISTANCE.txt BEAM_ENERGY.txt
do
    chmod g+w $f
done

cd ..
