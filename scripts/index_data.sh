#!/bin/bash
##SBATCH -p hour
##SBATCH -t 1:00:00
#SBATCH -e output/index_data.%j.err
#SBATCH -o output/index_data.%j.out
##SBATCH -x sf-cn-[5-7]

. ./env_setup.sh

DIRN=$1
RUN=$2
FILELST=$3
PROTEIN_NAME=$4
UNIQUERUN=$5

OUTDIR=`echo ${DIRN} | sed 's/data/index/'`

FR=`printf "%04d" ${RUN}`
FILEN=acq${FR}

RUNNR=`echo ${DIRN} | awk -F "/" '{print $1}' | awk -F "-" '{print $1}'`

hostname

if [ -e ${FILELST} ]
then

    mkdir -p ${BASEDIR}/${OUTDIR}

    cd ${BASEDIR}/${OUTDIR}

    . ${DIR_AP}/scripts/load_crystfel.sh

    cp ${CONFIG_DIR}/${GEOM_FILE}  ${FILEN}.geom

    grep ${RUNNR} ${CONFIG_DIR}/DETECTOR_DISTANCE.txt > /dev/null
    if [ $? = 0 ]
    then
       DETECTOR_DISTANCE=`grep ${RUNNR} ${CONFIG_DIR}/DETECTOR_DISTANCE.txt | tail -1 | awk '{print $2}'`
    else
       DETECTOR_DISTANCE=`grep DEFAULT ${CONFIG_DIR}/DETECTOR_DISTANCE.txt | tail -1 | awk '{print $2}'`
    fi 
    sed -i "s:clen.*:clen = ${DETECTOR_DISTANCE}:g" ${FILEN}.geom

    grep ${RUNNR} ${CONFIG_DIR}/BEAM_ENERGY.txt > /dev/null
    if [ $? = 0 ]
    then
       BEAM_ENERGY=`grep ${RUNNR} ${CONFIG_DIR}/BEAM_ENERGY.txt | tail -1 | awk '{print $2}'`
    else
       BEAM_ENERGY=`grep DEFAULT ${CONFIG_DIR}/BEAM_ENERGY.txt | tail -1 | awk '{print $2}'`
    fi
    sed -i "s:photon_energy.*:photon_energy = ${BEAM_ENERGY}:g" ${FILEN}.geom

    if [ -e ${CONFIG_DIR}/CELL/${PROTEIN_NAME}.cell ]
    then
        cp ${CONFIG_DIR}/CELL/${PROTEIN_NAME}.cell      ${FILEN}.cell
    fi

    rm -rf ${FILEN}.stream

    if [ -e ${CONFIG_DIR}/run_index.${PROTEIN_NAME}.sh ]
    then
        ${CONFIG_DIR}/run_index.${PROTEIN_NAME}.sh ${FILELST} ${FILEN}.geom ${FILEN}.cell ${FILEN}.stream > ${FILEN}.out 2>${FILEN}.err
    else
        ${CONFIG_DIR}/run_index.sh                 ${FILELST} ${FILEN}.geom ${FILEN}.cell ${FILEN}.stream > ${FILEN}.out 2>${FILEN}.err
    fi

    module purge

    laser=dark
    echo ${FILELST} | grep light.lst > /dev/null
    if [ $? = 0 ]
    then
        laser=light
    fi
    grep Final ${FILEN}.err > /dev/null
    if [ $? = 0 ]
    then
        n_indexed=`grep Final ${FILEN}.err | awk '{print $8}'`
        echo python ${DIR_AP}/ap/update-spreadsheet.py --url ${LOGBOOK} --unique_run ${UNIQUERUN} --laser ${laser} --number_indexed ${n_indexed} --credentials ${CONFIG_DIR}/credentials.json
        python ${DIR_AP}/ap/update-spreadsheet.py --url ${LOGBOOK} --unique_run ${UNIQUERUN} --laser ${laser} --number_indexed ${n_indexed} --credentials ${CONFIG_DIR}/credentials.json 
    fi

    ${DIR_AP}/scripts/ave-resolution ${FILEN}.stream > ${FILEN}.resolution.tmp
    if [ $? == 0 ]
    then 
       mean_res=`grep Mean   ${FILEN}.resolution.tmp | awk '{print $5}'` 
       best_res=`grep Best   ${FILEN}.resolution.tmp | awk '{print $5}'`
       worst_res=`grep Worst ${FILEN}.resolution.tmp | awk '{print $5}'`
       echo $mean_res $best_res $worst_res > ${FILEN}.resolution
    else
       echo "nan nan nan" > ${FILEN}.resolution
    fi

    read r1 r2 r3 <<< $(cat ${FILEN}.resolution)
    echo python ${DIR_AP}/ap/update-spreadsheet.py --url ${LOGBOOK} --unique_run ${UNIQUERUN} --laser ${laser} --resolution_min ${r3} --resolution_max ${r2} --resolution_mean ${r1} --credentials ${CONFIG_DIR}/credentials.json
    python ${DIR_AP}/ap/update-spreadsheet.py --url ${LOGBOOK} --unique_run ${UNIQUERUN} --laser ${laser} --resolution_min ${r3} --resolution_max ${r2} --resolution_mean ${r1} --credentials ${CONFIG_DIR}/credentials.json

    if [ -e ${CONFIG_DIR}/CELL/${PROTEIN_NAME}.cell_alternative ]
    then
        echo "Running alternative cell indexing"

        . ${DIR_AP}/scripts/load_crystfel.sh

            cp ${CONFIG_DIR}/CELL/${PROTEIN_NAME}.cell_alternative ${FILEN}.cell_alternative
            diff ${FILEN}.cell ${FILEN}.cell_alternative > /dev/null
            if [ $? != 0 ]
            then
                rm -rf ${FILEN}.stream_alternative

                if [ -e ${CONFIG_DIR}/run_index.${PROTEIN_NAME}.sh ]
                then
                    ${CONFIG_DIR}/run_index.${PROTEIN_NAME}.sh ${FILELST} ${FILEN}.geom ${FILEN}.cell_alternative ${FILEN}.stream_alternative > ${FILEN}.out_alternative 2>${FILEN}.err_alternative
                else
                    ${CONFIG_DIR}/run_index.sh ${FILELST} ${FILEN}.geom ${FILEN}.cell_alternative ${FILEN}.stream_alternative > ${FILEN}.out_alternative 2>${FILEN}.err_alternative
                fi
                rm -rf ${FILEN}.cell_alternative

                grep Final ${FILEN}.err_alternative > /dev/null
                if [ $? = 0 ]
                then
                    n_indexed_alternative=`grep Final ${FILEN}.err_alternative | awk '{print $8}'`
                    echo python ${DIP_AP}/ap/update-spreadsheet.py --url ${LOGBOOK} --unique_run ${UNIQUERUN} --laser ${laser} --number_indexed_alternative ${n_indexed_alternative} --credentials ${CONFIG_DIR}/credentials.json
                    python ${DIR_AP}/ap/update-spreadsheet.py --url ${LOGBOOK} --unique_run ${UNIQUERUN} --laser ${laser} --number_indexed_alternative ${n_indexed_alternative} --credentials ${CONFIG_DIR}/credentials.json
                fi

            else
                echo "Alternative cell is the same as main cell"
            fi

    fi

    rm -rf ${FILEN}.list ${FILEN}.geom ${FILEN}.cell ${FILEN}.resolution.tmp

else
    
   echo "File ${FILELST} doesnt exists"

fi

