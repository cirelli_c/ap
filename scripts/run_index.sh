#!/bin/bash

NP=`grep processor /proc/cpuinfo | wc -l`

if [ $# -ge 1 ]
then
    FILELST=$1
else
    FILELST=file.lst
fi

if [ $# -ge 2 ]
then
    FILEGEOM=$2
else
    FILEGEOM=geom.geom
fi

if [ $# -ge 3 ]
then
    FILECELL=$3
else
    FILECELL=cell.cell
fi

if [ $# -ge 4 ]
then
    FILESTREAM=$4
else
    FILESTREAM=stream.stream
fi

# list of indexing methods : xgandalf-latt-cell,asdf-latt-cell,dirax-latt-cell,mosflm-latt-cell
# but usually all is indexed by xgandalf (99.99%)

# to speed up indexing, one can use : --xgandalf-fast-execution, --highres=1.6 ...

if [ -e ${FILECELL} ]
then

    indexamajig -i ${FILELST} \
    -o ${FILESTREAM} \
    --geometry=${FILEGEOM} \
    --pdb=${FILECELL} \
    --indexing=xgandalf-latt-cell \
    --peaks=peakfinder8 \
    --integration=rings-grad \
    --tolerance=10.0,10.0,10.0,2,3,2 \
    --threshold=2000 \
    --min-snr=5 \
    --int-radius=2,3,6 \
    -j ${NP} \
    --no-multi \
    --no-retry \
    --check-peaks \
    --max-res=3000 \
    --min-pix-count=1 \
    --local-bg-radius=4 

else

    indexamajig -i ${FILELST} \
    -o ${FILESTREAM} \
    --geometry=${FILEGEOM} \
    --indexing=xgandalf-latt-cell \
    --peaks=peakfinder8 \
    --integration=rings-grad \
    --tolerance=10.0,10.0,10.0,2,2,2 \
    --threshold=2000 \
    --min-snr=5 \
    --int-radius=2,3,6 \
    -j ${NP} \
    --no-multi \
    --no-retry \
    --check-peaks \
    --max-res=3000 \
    --min-pix-count=1 \
    --local-bg-radius=4 

fi

