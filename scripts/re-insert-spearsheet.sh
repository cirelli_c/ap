#!/bin/bash

cd output

mkdir -p re-try

for i in {0001..5000}
do 
  for l in light dark
  do
    f=run00${i}.index.${l}.slurm.out
    if [ -e ${f} ]
    then 
      grep -v docs.google ${f} | egrep " google|Cannot find" > /dev/null
      a=$?
      b=1
      if [ -s run00${i}.index.${l}.slurm.err ]
      then
          b=0
      fi
      if [ $a == 0 ] || [ $b == 0 ]
      then 
        grep python $f | grep credentials.json > a
        if [ -s a ]
        then
            chmod +x a
            ./a > run00${i}.index.${l}.slurm.err
            grep -v " google" $f | grep -v "Cannot find" > b
            mv b $f
            mv a re-try/run00${i}.`date +%s`.sh
        fi
        rm -rf a
      fi
    fi
  done
done
cd ..
