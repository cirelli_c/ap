#!/bin/bash

. ./env_setup.sh

if [ -e STOP ]
then
    echo "STOP file present - exiting AP now"
    echo "to run AP, remove STOP file with rm STOP"
    exit
fi

while true
do 

    if [ -e STOP ]
    then
        echo "STOP file present - exiting AP now"
        echo "to run AP, remove STOP file with rm STOP"
        exit
    fi

    echo "Running "`date`
    python ${DIR_AP}/ap/ap.py --pgroup ${PGROUP} --beamline ${BEAMLINE} --detector ${DETECTOR_NAME} --logbook ${LOGBOOK} --dir_ap ${DIR_AP}

    ${DIR_AP}/scripts/jobs_overview.sh

    ${DIR_AP}/scripts/re-insert-spearsheet.base.sh

    ${DIR_AP}/scripts/re-insert-spearsheet.sh

    ${DIR_AP}/scripts/re-insert-spearsheet.2.sh

    echo "Sleeping  "`date`
    sleep 120
done
